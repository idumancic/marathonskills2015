﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarathonSkills2015.Desktop.Helpers
{
    public class TimeManager
    {
        private const int YEAR = 2017;
        private const int MONTH = 10;
        private const int DAY = 11;
        private const int HOUR = 6;
        private const int MINUTE = 0;
        private const int SECOND = 0;
        private static readonly DateTime startDate = new DateTime(YEAR, MONTH, DAY, HOUR, MINUTE, SECOND);

        public static void SetLabelTime(Label label)
        {
            TimeSpan untilStart = startDate - DateTime.Now;
            label.Text = string.Format("{0} days {1} hours {2} minutes until the race starts", untilStart.Days, untilStart.Hours, untilStart.Minutes);
        }
    }
}
