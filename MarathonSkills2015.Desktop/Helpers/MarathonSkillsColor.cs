﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarathonSkills2015.Desktop.Helpers
{
    class MarathonSkillsColor
    {
        //Primary colours
        public static readonly Color BrazilGreen = Color.FromArgb(0, 144, 62);
        public static readonly Color BrazilYellow = Color.FromArgb(253, 193, 0);
        public static readonly Color BrazilBlue = Color.FromArgb(36, 29, 112);

        //Secondary colours
        public static readonly Color LightGrey = Color.FromArgb(235, 235, 235);
        public static readonly Color MidGrey = Color.FromArgb(180, 180, 180);
        public static readonly Color DarkGrey = Color.FromArgb(80, 80, 80);

    }
}
