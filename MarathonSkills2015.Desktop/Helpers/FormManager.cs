﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarathonSkills2015.Desktop.Helpers
{
    public class FormManager
    {
        public static Form LastActiveForm { get; set; }

        public static void Show(Form parentForm, Type instanceType)
        {
            parentForm.Hide();
            LastActiveForm = parentForm;

            Form childForm = Activator.CreateInstance(instanceType) as Form;
            InitializeForm(childForm);
        }

        private static void InitializeForm(Form childForm)
        {
            childForm.MaximizeBox = false;
            childForm.FormBorderStyle = FormBorderStyle.FixedSingle;
            childForm.StartPosition = FormStartPosition.Manual;
            childForm.Location = LastActiveForm.Location;
            childForm.Show();
        }

        public static void ShowParent(Form childForm)
        {
            childForm.Hide();

            LastActiveForm.Location = childForm.Location;
            LastActiveForm.Show();

            LastActiveForm = childForm;
        }

    }
}
