﻿namespace MarathonSkills2015.Desktop.Forms
{
    partial class SponsorRunner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flpLabelContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.lblSponsorARunner = new System.Windows.Forms.Label();
            this.lblChooseARunnerDesc = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tblSponsorshipDetails = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSponsorName = new System.Windows.Forms.Label();
            this.txtSponsorName = new System.Windows.Forms.TextBox();
            this.lblSponsorshipDetails = new System.Windows.Forms.Label();
            this.txtNameOnCard = new System.Windows.Forms.TextBox();
            this.txtCreditCard = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.txtExpiryMonth = new System.Windows.Forms.TextBox();
            this.txtExpiryYear = new System.Windows.Forms.TextBox();
            this.txtCVC = new System.Windows.Forms.TextBox();
            this.cbRunners = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.flpLabelContainer.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tblSponsorshipDetails.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flpLabelContainer
            // 
            this.flpLabelContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpLabelContainer.BackColor = System.Drawing.Color.Transparent;
            this.flpLabelContainer.Controls.Add(this.lblSponsorARunner);
            this.flpLabelContainer.Controls.Add(this.lblChooseARunnerDesc);
            this.flpLabelContainer.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpLabelContainer.Location = new System.Drawing.Point(1, 58);
            this.flpLabelContainer.Name = "flpLabelContainer";
            this.flpLabelContainer.Size = new System.Drawing.Size(1183, 120);
            this.flpLabelContainer.TabIndex = 5;
            // 
            // lblSponsorARunner
            // 
            this.lblSponsorARunner.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSponsorARunner.Location = new System.Drawing.Point(3, 0);
            this.lblSponsorARunner.Name = "lblSponsorARunner";
            this.lblSponsorARunner.Size = new System.Drawing.Size(1180, 60);
            this.lblSponsorARunner.TabIndex = 0;
            this.lblSponsorARunner.Text = "Sponsor a runner";
            this.lblSponsorARunner.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblChooseARunnerDesc
            // 
            this.lblChooseARunnerDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChooseARunnerDesc.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChooseARunnerDesc.Location = new System.Drawing.Point(3, 60);
            this.lblChooseARunnerDesc.Name = "lblChooseARunnerDesc";
            this.lblChooseARunnerDesc.Size = new System.Drawing.Size(1180, 60);
            this.lblChooseARunnerDesc.TabIndex = 1;
            this.lblChooseARunnerDesc.Text = "Please choose the runner you would like to sponsor and the amount you would like " +
    "\r\nto sponsor them for. Thank you for your support of the runners and their chari" +
    "ties!";
            this.lblChooseARunnerDesc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.panel1);
            this.flowLayoutPanel2.Controls.Add(this.panel2);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(1, 184);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1183, 421);
            this.flowLayoutPanel2.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.tblSponsorshipDetails);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(579, 418);
            this.panel1.TabIndex = 0;
            // 
            // tblSponsorshipDetails
            // 
            this.tblSponsorshipDetails.ColumnCount = 2;
            this.tblSponsorshipDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tblSponsorshipDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tblSponsorshipDetails.Controls.Add(this.label1, 0, 2);
            this.tblSponsorshipDetails.Controls.Add(this.lblSponsorName, 0, 1);
            this.tblSponsorshipDetails.Controls.Add(this.txtSponsorName, 1, 1);
            this.tblSponsorshipDetails.Controls.Add(this.lblSponsorshipDetails, 0, 0);
            this.tblSponsorshipDetails.Controls.Add(this.txtNameOnCard, 1, 3);
            this.tblSponsorshipDetails.Controls.Add(this.txtCreditCard, 1, 4);
            this.tblSponsorshipDetails.Controls.Add(this.label2, 0, 3);
            this.tblSponsorshipDetails.Controls.Add(this.label3, 0, 4);
            this.tblSponsorshipDetails.Controls.Add(this.label4, 0, 5);
            this.tblSponsorshipDetails.Controls.Add(this.label5, 0, 6);
            this.tblSponsorshipDetails.Controls.Add(this.flowLayoutPanel1, 1, 5);
            this.tblSponsorshipDetails.Controls.Add(this.txtCVC, 1, 6);
            this.tblSponsorshipDetails.Controls.Add(this.cbRunners, 1, 2);
            this.tblSponsorshipDetails.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tblSponsorshipDetails.Location = new System.Drawing.Point(25, 22);
            this.tblSponsorshipDetails.Name = "tblSponsorshipDetails";
            this.tblSponsorshipDetails.RowCount = 7;
            this.tblSponsorshipDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tblSponsorshipDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tblSponsorshipDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tblSponsorshipDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tblSponsorshipDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tblSponsorshipDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tblSponsorshipDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tblSponsorshipDetails.Size = new System.Drawing.Size(528, 350);
            this.tblSponsorshipDetails.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 50);
            this.label1.TabIndex = 6;
            this.label1.Text = "Runner:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSponsorName
            // 
            this.lblSponsorName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSponsorName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSponsorName.Location = new System.Drawing.Point(3, 50);
            this.lblSponsorName.Name = "lblSponsorName";
            this.lblSponsorName.Size = new System.Drawing.Size(152, 50);
            this.lblSponsorName.TabIndex = 2;
            this.lblSponsorName.Text = "Your Name:";
            this.lblSponsorName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSponsorName
            // 
            this.txtSponsorName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSponsorName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSponsorName.Location = new System.Drawing.Point(161, 62);
            this.txtSponsorName.Name = "txtSponsorName";
            this.txtSponsorName.Size = new System.Drawing.Size(364, 26);
            this.txtSponsorName.TabIndex = 0;
            // 
            // lblSponsorshipDetails
            // 
            this.tblSponsorshipDetails.SetColumnSpan(this.lblSponsorshipDetails, 2);
            this.lblSponsorshipDetails.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.lblSponsorshipDetails.Location = new System.Drawing.Point(3, 0);
            this.lblSponsorshipDetails.Name = "lblSponsorshipDetails";
            this.lblSponsorshipDetails.Size = new System.Drawing.Size(522, 50);
            this.lblSponsorshipDetails.TabIndex = 3;
            this.lblSponsorshipDetails.Text = "Sponsoship details";
            this.lblSponsorshipDetails.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNameOnCard
            // 
            this.txtNameOnCard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNameOnCard.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNameOnCard.Location = new System.Drawing.Point(161, 162);
            this.txtNameOnCard.Name = "txtNameOnCard";
            this.txtNameOnCard.Size = new System.Drawing.Size(364, 26);
            this.txtNameOnCard.TabIndex = 5;
            // 
            // txtCreditCard
            // 
            this.txtCreditCard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCreditCard.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditCard.Location = new System.Drawing.Point(161, 212);
            this.txtCreditCard.Name = "txtCreditCard";
            this.txtCreditCard.Size = new System.Drawing.Size(364, 26);
            this.txtCreditCard.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 50);
            this.label2.TabIndex = 7;
            this.label2.Text = "Name on Card:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 50);
            this.label3.TabIndex = 8;
            this.label3.Text = "Credit Card #:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 250);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 50);
            this.label4.TabIndex = 9;
            this.label4.Text = "Expiry Date:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 300);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 50);
            this.label5.TabIndex = 10;
            this.label5.Text = "CVC:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.txtExpiryMonth);
            this.flowLayoutPanel1.Controls.Add(this.txtExpiryYear);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(161, 259);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(364, 32);
            this.flowLayoutPanel1.TabIndex = 11;
            // 
            // txtExpiryMonth
            // 
            this.txtExpiryMonth.Location = new System.Drawing.Point(3, 3);
            this.txtExpiryMonth.Name = "txtExpiryMonth";
            this.txtExpiryMonth.Size = new System.Drawing.Size(44, 26);
            this.txtExpiryMonth.TabIndex = 0;
            // 
            // txtExpiryYear
            // 
            this.txtExpiryYear.Location = new System.Drawing.Point(53, 3);
            this.txtExpiryYear.Name = "txtExpiryYear";
            this.txtExpiryYear.Size = new System.Drawing.Size(68, 26);
            this.txtExpiryYear.TabIndex = 1;
            // 
            // txtCVC
            // 
            this.txtCVC.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtCVC.Location = new System.Drawing.Point(161, 312);
            this.txtCVC.Name = "txtCVC";
            this.txtCVC.Size = new System.Drawing.Size(68, 26);
            this.txtCVC.TabIndex = 12;
            // 
            // cbRunners
            // 
            this.cbRunners.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbRunners.FormattingEnabled = true;
            this.cbRunners.Location = new System.Drawing.Point(161, 114);
            this.cbRunners.Name = "cbRunners";
            this.cbRunners.Size = new System.Drawing.Size(364, 26);
            this.cbRunners.TabIndex = 13;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Location = new System.Drawing.Point(588, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(583, 418);
            this.panel2.TabIndex = 1;
            // 
            // SponsorRunner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flpLabelContainer);
            this.Name = "SponsorRunner";
            this.Controls.SetChildIndex(this.flpLabelContainer, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel2, 0);
            this.flpLabelContainer.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tblSponsorshipDetails.ResumeLayout(false);
            this.tblSponsorshipDetails.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpLabelContainer;
        private System.Windows.Forms.Label lblSponsorARunner;
        private System.Windows.Forms.Label lblChooseARunnerDesc;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tblSponsorshipDetails;
        private System.Windows.Forms.TextBox txtSponsorName;
        private System.Windows.Forms.Label lblSponsorName;
        private System.Windows.Forms.Label lblSponsorshipDetails;
        private System.Windows.Forms.TextBox txtNameOnCard;
        private System.Windows.Forms.TextBox txtCreditCard;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TextBox txtExpiryMonth;
        private System.Windows.Forms.TextBox txtExpiryYear;
        private System.Windows.Forms.TextBox txtCVC;
        private System.Windows.Forms.ComboBox cbRunners;
    }
}
