﻿namespace MarathonSkills2015.Desktop.Forms
{
    partial class MainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlMainLabels = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblMarathonSkills2015 = new System.Windows.Forms.Label();
            this.lblPlace = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnBeARunner = new System.Windows.Forms.Button();
            this.btnSponsorARunner = new System.Windows.Forms.Button();
            this.btnFindOutMore = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblTimeUntilRaceStarts = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.flowLayoutPanel1.SuspendLayout();
            this.pnlMainLabels.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel1.Controls.Add(this.pnlMainLabels);
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Controls.Add(this.lblTimeUntilRaceStarts);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-2, -2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1189, 665);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // pnlMainLabels
            // 
            this.pnlMainLabels.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMainLabels.Controls.Add(this.flowLayoutPanel2);
            this.pnlMainLabels.Location = new System.Drawing.Point(3, 3);
            this.pnlMainLabels.Name = "pnlMainLabels";
            this.pnlMainLabels.Size = new System.Drawing.Size(1186, 150);
            this.pnlMainLabels.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.lblMarathonSkills2015);
            this.flowLayoutPanel2.Controls.Add(this.lblPlace);
            this.flowLayoutPanel2.Controls.Add(this.lblInfo);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1180, 144);
            this.flowLayoutPanel2.TabIndex = 0;
            // 
            // lblMarathonSkills2015
            // 
            this.lblMarathonSkills2015.BackColor = System.Drawing.Color.White;
            this.lblMarathonSkills2015.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblMarathonSkills2015.Location = new System.Drawing.Point(3, 0);
            this.lblMarathonSkills2015.Name = "lblMarathonSkills2015";
            this.lblMarathonSkills2015.Size = new System.Drawing.Size(1177, 67);
            this.lblMarathonSkills2015.TabIndex = 0;
            this.lblMarathonSkills2015.Text = "MARATHON SKILLS 2015";
            this.lblMarathonSkills2015.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPlace
            // 
            this.lblPlace.BackColor = System.Drawing.Color.White;
            this.lblPlace.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPlace.Location = new System.Drawing.Point(3, 67);
            this.lblPlace.Name = "lblPlace";
            this.lblPlace.Size = new System.Drawing.Size(1177, 37);
            this.lblPlace.TabIndex = 1;
            this.lblPlace.Text = "Sao Paulo, Brazil";
            this.lblPlace.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInfo
            // 
            this.lblInfo.BackColor = System.Drawing.Color.White;
            this.lblInfo.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblInfo.ForeColor = System.Drawing.Color.Black;
            this.lblInfo.Location = new System.Drawing.Point(3, 104);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(1171, 40);
            this.lblInfo.TabIndex = 2;
            this.lblInfo.Text = "Saturday September 5 2015";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.Controls.Add(this.flowLayoutPanel3);
            this.panel2.Location = new System.Drawing.Point(3, 159);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1186, 450);
            this.panel2.TabIndex = 1;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel3.Controls.Add(this.panel3);
            this.flowLayoutPanel3.Controls.Add(this.panel4);
            this.flowLayoutPanel3.Controls.Add(this.panel5);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1183, 450);
            this.flowLayoutPanel3.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(350, 444);
            this.panel3.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.flowLayoutPanel4);
            this.panel4.Location = new System.Drawing.Point(359, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(465, 444);
            this.panel4.TabIndex = 1;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.btnBeARunner);
            this.flowLayoutPanel4.Controls.Add(this.btnSponsorARunner);
            this.flowLayoutPanel4.Controls.Add(this.btnFindOutMore);
            this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(3, 98);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(459, 231);
            this.flowLayoutPanel4.TabIndex = 0;
            // 
            // btnBeARunner
            // 
            this.btnBeARunner.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnBeARunner.Location = new System.Drawing.Point(3, 3);
            this.btnBeARunner.Name = "btnBeARunner";
            this.btnBeARunner.Size = new System.Drawing.Size(456, 70);
            this.btnBeARunner.TabIndex = 0;
            this.btnBeARunner.Text = "I want to be a runner";
            this.btnBeARunner.UseVisualStyleBackColor = true;
            // 
            // btnSponsorARunner
            // 
            this.btnSponsorARunner.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSponsorARunner.Location = new System.Drawing.Point(3, 79);
            this.btnSponsorARunner.Name = "btnSponsorARunner";
            this.btnSponsorARunner.Size = new System.Drawing.Size(456, 70);
            this.btnSponsorARunner.TabIndex = 2;
            this.btnSponsorARunner.Text = "I want to sponsor a runner";
            this.btnSponsorARunner.UseVisualStyleBackColor = true;
            this.btnSponsorARunner.Click += new System.EventHandler(this.btnSponsorARunner_Click);
            // 
            // btnFindOutMore
            // 
            this.btnFindOutMore.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnFindOutMore.Location = new System.Drawing.Point(3, 155);
            this.btnFindOutMore.Name = "btnFindOutMore";
            this.btnFindOutMore.Size = new System.Drawing.Size(456, 70);
            this.btnFindOutMore.TabIndex = 3;
            this.btnFindOutMore.Text = "I want to find out more";
            this.btnFindOutMore.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.btnLogin);
            this.panel5.Location = new System.Drawing.Point(830, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(350, 444);
            this.panel5.TabIndex = 2;
            // 
            // btnLogin
            // 
            this.btnLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogin.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnLogin.Location = new System.Drawing.Point(238, 401);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(109, 40);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            // 
            // lblTimeUntilRaceStarts
            // 
            this.lblTimeUntilRaceStarts.BackColor = System.Drawing.Color.White;
            this.lblTimeUntilRaceStarts.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTimeUntilRaceStarts.Location = new System.Drawing.Point(3, 612);
            this.lblTimeUntilRaceStarts.Name = "lblTimeUntilRaceStarts";
            this.lblTimeUntilRaceStarts.Size = new System.Drawing.Size(1186, 53);
            this.lblTimeUntilRaceStarts.TabIndex = 2;
            this.lblTimeUntilRaceStarts.Text = "label1";
            this.lblTimeUntilRaceStarts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // MainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.Name = "MainScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Marathon Skills 2015";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.pnlMainLabels.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel pnlMainLabels;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblTimeUntilRaceStarts;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label lblMarathonSkills2015;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Button btnBeARunner;
        private System.Windows.Forms.Button btnSponsorARunner;
        private System.Windows.Forms.Button btnFindOutMore;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lblPlace;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Timer timer;
    }
}