﻿using MarathonSkills2015.Desktop.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarathonSkills2015.Desktop.BaseForms
{
    public partial class BackButton : Form
    {
        public BackButton()
        {
            InitializeComponent();
            InitializeForm();
        }

        private void InitializeForm()
        {
            TimeManager.SetLabelTime(lblTimeUntilRaceStarts);
            lblTimeUntilRaceStarts.BackColor = MarathonSkillsColor.DarkGrey;
            lblTimeUntilRaceStarts.ForeColor = Color.White;

            lblTitle.BackColor = MarathonSkillsColor.DarkGrey;
            lblTitle.ForeColor = Color.White;

            flpContainer.BackColor = MarathonSkillsColor.DarkGrey;
            pnlButtonContainer.BackColor = MarathonSkillsColor.DarkGrey;

            btnBack.BackColor = MarathonSkillsColor.LightGrey;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            FormManager.ShowParent(this);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            TimeManager.SetLabelTime(lblTimeUntilRaceStarts);
        }
    }
}
