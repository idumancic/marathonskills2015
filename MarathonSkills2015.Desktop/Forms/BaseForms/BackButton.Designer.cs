﻿namespace MarathonSkills2015.Desktop.BaseForms
{
    partial class BackButton
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flpContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlButtonContainer = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblTimeUntilRaceStarts = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.flpContainer.SuspendLayout();
            this.pnlButtonContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // flpContainer
            // 
            this.flpContainer.Controls.Add(this.pnlButtonContainer);
            this.flpContainer.Controls.Add(this.lblTitle);
            this.flpContainer.Location = new System.Drawing.Point(1, 0);
            this.flpContainer.Name = "flpContainer";
            this.flpContainer.Size = new System.Drawing.Size(1183, 55);
            this.flpContainer.TabIndex = 0;
            // 
            // pnlButtonContainer
            // 
            this.pnlButtonContainer.Controls.Add(this.btnBack);
            this.pnlButtonContainer.Location = new System.Drawing.Point(3, 3);
            this.pnlButtonContainer.Name = "pnlButtonContainer";
            this.pnlButtonContainer.Size = new System.Drawing.Size(106, 52);
            this.pnlButtonContainer.TabIndex = 0;
            // 
            // btnBack
            // 
            this.btnBack.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(8, 9);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(89, 31);
            this.btnBack.TabIndex = 0;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(115, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(1043, 55);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "MARATHON SKILLS 2015";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTimeUntilRaceStarts
            // 
            this.lblTimeUntilRaceStarts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTimeUntilRaceStarts.BackColor = System.Drawing.Color.White;
            this.lblTimeUntilRaceStarts.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTimeUntilRaceStarts.Location = new System.Drawing.Point(-2, 608);
            this.lblTimeUntilRaceStarts.Name = "lblTimeUntilRaceStarts";
            this.lblTimeUntilRaceStarts.Size = new System.Drawing.Size(1186, 53);
            this.lblTimeUntilRaceStarts.TabIndex = 4;
            this.lblTimeUntilRaceStarts.Text = "0 days 0 hours and 0 minutes until the race starts!";
            this.lblTimeUntilRaceStarts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // BackButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.lblTimeUntilRaceStarts);
            this.Controls.Add(this.flpContainer);
            this.Name = "BackButton";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "BackButton";
            this.flpContainer.ResumeLayout(false);
            this.pnlButtonContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flpContainer;
        private System.Windows.Forms.Panel pnlButtonContainer;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblTimeUntilRaceStarts;
        private System.Windows.Forms.Timer timer;
    }
}