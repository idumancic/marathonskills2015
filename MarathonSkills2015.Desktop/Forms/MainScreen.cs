﻿using MarathonSkills2015.Desktop.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarathonSkills2015.Desktop.Forms
{
    public partial class MainScreen : Form
    {
        public MainScreen()
        {
            InitializeComponent();
            InitializeMainScreen();
        }

        private void InitializeMainScreen()
        {
            FormBorderStyle = FormBorderStyle.FixedSingle;
            TimeManager.SetLabelTime(lblTimeUntilRaceStarts);

            pnlMainLabels.BackColor = MarathonSkillsColor.DarkGrey;

            lblMarathonSkills2015.BackColor = MarathonSkillsColor.DarkGrey;
            lblMarathonSkills2015.ForeColor = Color.White;

            lblInfo.BackColor = MarathonSkillsColor.DarkGrey;
            lblInfo.ForeColor = MarathonSkillsColor.MidGrey;

            lblPlace.BackColor = MarathonSkillsColor.DarkGrey;
            lblPlace.ForeColor = MarathonSkillsColor.MidGrey;

            btnBeARunner.BackColor = MarathonSkillsColor.LightGrey;
            btnFindOutMore.BackColor = MarathonSkillsColor.LightGrey;
            btnSponsorARunner.BackColor = MarathonSkillsColor.LightGrey;
            btnLogin.BackColor = MarathonSkillsColor.LightGrey;

            lblTimeUntilRaceStarts.BackColor = MarathonSkillsColor.DarkGrey;
            lblTimeUntilRaceStarts.ForeColor = Color.White;

            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            TimeManager.SetLabelTime(lblTimeUntilRaceStarts);
        }

        private void btnSponsorARunner_Click(object sender, EventArgs e)
        {
            FormManager.Show(this, typeof(SponsorRunner));
        }
    }
}
