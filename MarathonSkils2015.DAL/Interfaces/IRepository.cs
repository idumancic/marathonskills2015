﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarathonSkils2015.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        void Add(T item);
        void Update(T item);
        void Remove(int id);
        IEnumerable<T> GetAll();
        T Get(int id);
    }
}
